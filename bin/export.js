#!/usr/bin/env node

const chalk = require('chalk')
const inquirer = require('inquirer')
const sh = require('shelljs')

inquirer
  .prompt([
    {
      name: 'host',
      message: 'MySQL Server Host (Default: "0.0.0.0" = localhost',
      default: "0.0.0.0"
    },
    {
      name: 'user',
      message: 'MySQL Server User',
      default: "root"
    },
    {
      name: 'password',
      type: 'password',
      message: 'MySQL Server Password (default: "root")',
      default: "root"
    },
    {
      name: 'database',
      message: 'MySQL Server Database',
      validate: (input) => (input.length > 0) ? true : "You must specify the database to export."
    },
    {
      name: 'includeData',
      message: `Export the Schema only? (y/n)`,
      default: "No"
    },
    {
      name: 'dropTables',
      message: `Include ${chalk.red("DROP TABLE")} statement at the start of the dump? (y/n)`,
      default: "Yes"
    }
  ])
  .then(({host, password, user, database, includeData: withoutData, dropTables}) => {
      withoutData = (withoutData.toUpperCase() === "Y" || withoutData.toUpperCase() === "YES")
      dropTables = (dropTables.toUpperCase() === "Y" || dropTables.toUpperCase() === "YES")
      inquirer
        .prompt([
          {
            name: 'file',
            message: 'Output .sql File Name',
            default: database,
            transformer: input => {
              if (!input.endsWith(".sql"))
                return input + ".sql"
              return input

            },
            filter: input => {
              if (!input.endsWith(".sql"))
                return input + ".sql"
              return input
            },
          }
        ])
        .then(({file}) => {
            let options = ['--verbose'];
            if (withoutData) {
              options.push('--no-data')
            }

            if (!dropTables) {
              options.push('--skip-add-drop-table')
            }
            let command = `docker run --rm --network=host mysql:5.7 /usr/bin/mysqldump ${options.join(" ")} --host=${host} --port=3306 --user=${user} --password=${password} ${database} > ${file}`;
            console.log("Running command: ", command)
            sh.exec(command)
          }
        )
    }
  )
